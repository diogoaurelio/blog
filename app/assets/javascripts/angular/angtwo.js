

var appTwo = angular.module('appTwo', ['controllers4']);


angular.module('controllers4', []).controller('MoreController', function($scope) {
	$scope.num = 0
	$scope.nums = []

	$scope.valuetest = "123abc"
	
	$scope.increment = function() {
		$scope.num++;
	}
	// The $scope.breakit breaks the watcher when it is not needed anymore:
	$scope.breakit = $scope.$watch('num', function(){
		$scope.nums.push($scope.num)
	})
	
	$scope.somedata = "How to prevent browser from displaying raw AngularJS code on initial page load"	

})

