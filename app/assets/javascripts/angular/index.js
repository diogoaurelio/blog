
var app = angular.module('app', ['controllers', 'controllers2', 'controllers3', 'controllers4']);

// First way to declare a Controller, not the recommended:
/*
var MainController = function($scope) {
	$scope.val = "First Value tested. Next, a function:"
	$scope.func = function() {
		return "abc" + "def"
	}
}

*/
// This option isn't the prefered as well
/*
app.controller('MainController', function($scope) {
	$scope.val = "test 456"
})
*/

//Third way is declare the controler within a Module
angular.module('controllers', []).controller('MainController', function($scope) {
	$scope.val = 1
	$scope.even = false
	
	$scope.inc = function() {
		$scope.val += 1
		$scope.even = $scope.val%2 == 0 //Returns true if even, false if odds
		
	}
})

angular.module('controllers2', []).controller('SecController', function($scope) {
	$scope. classVar = "orange"
	$scope.classVar2 = true
	$scope.myarr = [1,2,3,5,6,7,8,9]
	$scope.users = [{name:"mike", age:23}, {name:"andy", age:34}, {name:"reid", age:45}]
})

angular.module('controllers3', []).controller('ThirdController', function($scope) {
	$scope.datathree = {val : "Jake"}
	$scope.$watch('datathree.val', function(newval) {
		$scope.datathree.toolong = newval.length > 8
	})
})
